var btnMoins = document.querySelectorAll('.case-moins');
var btnPlus = document.querySelectorAll('.case-plus');
var inputQunatity = document.querySelectorAll('.case');
var removeData = document.querySelectorAll('.fa-trash-alt');
var heart = document.querySelectorAll('.fa-heart');

var prix = document.querySelectorAll('.prix span');
var quantity = document.querySelectorAll('.case');
var prixtotal = document.querySelectorAll('.prixtotal');
var product = document.querySelectorAll('.product');

for (const key in removeData) {
    if(removeData[key] && removeData[key].addEventListener){
        removeData[key].addEventListener('click', ()=> removeContent(key));
    }
}

for (const key in heart) {
    if(heart[key] && heart[key].addEventListener){
        heart[key].addEventListener('click', ()=> changeHeart(key));
    }
}

for (const key in inputQunatity) {
    if(inputQunatity[key] && inputQunatity[key].addEventListener){
        inputQunatity[key].addEventListener('keyup', ()=> changePrice(key));
    }
}

for (const key in btnPlus) {
    if(btnPlus[key] && btnPlus[key].addEventListener){
        btnPlus[key].addEventListener('click', ()=> increaseQuantity(key));
    }
}

for (const key in btnMoins) {
    if(btnMoins[key] && btnMoins[key].addEventListener){
        btnMoins[key].addEventListener('click', ()=> decreaseQuantity(key));
    }
}

function increaseQuantity(key) {
    const price = parseInt(prix[key]?.textContent ??  0);
    const quantityValue = quantity[key].value = parseInt(quantity[key]?.value) + 1;
    prixtotal[key].value = price * quantityValue;
}


function decreaseQuantity(key) {
    const price = parseInt(prix[key]?.textContent ??  0);
    if (quantity[key].value > 0) {
        const quantityValue = quantity[key].value = parseInt(quantity[key]?.value) - 1;
        prixtotal[key].value = price * quantityValue;
    }
}

function changePrice(key){
    const price = parseInt(prix[key]?.textContent ??  0);
    const quantityValue = quantity[key].value
    if(quantityValue > 0){
        prixtotal[key].value = price * quantityValue; 
    }
}

function removeContent(key){
    product[key].style.display = 'none';
}

function changeHeart(key){
    heart[key].style.color === "red" ?  heart[key].style.color = "black" : heart[key].style.color = "red";
}